import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class HelloTest {
    @Test
    public void testHelloReturnHello() {
        assertEquals(Hello.say("Hello"),"Hello");
    }

    @Test
    public void testByeReturnBye() {
        assertEquals(Hello.say("Bye"),"Bye");
    }

    @Ignore
    @Test
    public void testByeReturnFail() {
        assertEquals(Hello.say("Fail"),"Bye");
    }

    @Test
    public void testHiReturnHi3() {
        assertEquals(Hello.say("Hi3"),"Hi3");
    }
}